# Mellisuga #

#### a lightweight NodeJS based content management system and web development environment that aims for flexibility, performance, stability and ease of use. ####

##### also [Mellisuga is a genus of hummingbirds](https://en.wikipedia.org/wiki/Mellisuga) #####

###### WARNING: This is an experimental version. There might be serious security vulnerabilities and bugs. Reported issues and calaborators appreciated. ######

### Installation ###

`npm install mellisuga`

You also need to install `PostgreSQL` database.



### Quick start ###
Run:

```console
  git clone https://gitlab.com/subtledev/mellisuga-quick-start.git
  cd mellisuga-quick-start
  npm install
```

If you don't want to install `mellisuga` package because you have it locally, you shoud edit `package.json` and remove `mellisuga` from `dependencies`

