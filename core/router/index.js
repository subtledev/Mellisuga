

import http from 'http';
import path from 'path';
import bodyParser from 'body-parser'

import express from 'express';
import cookieParser from 'cookie-parser';

import Client from './client.js';

import cookie from 'cookie';

import compression from 'compression'


import multer from 'multer';

import { Server } from 'socket.io';

/**
 * High level wrap around exress enabling some mellisuga features.
 * @class Router
 * @hideconstructor
 */
export default class Router {
  /**
   * @static
   * @async
   * @method init
   * @memberof Router
   * @arg {String} host HTTP server adress i.e. `127.0.0.1`
   * @arg {Integer} port HTTP server port i.e. `8080`
   * @returns {Router}
   */
    static async init(host, port) {
      try {
        let app = express();
        let server = http.createServer(app);
        app.http_server = server;

        const io = new Server(server, {});
        io.csockets = [];
        io.on("connection", async (socket) => {
          try {
            console.log("SOCKET CONNECTED");
            io.csockets.push(socket);
            socket.on("disconnect", (reason) => {
              console.log("DISONNECT", reason);
              io.csockets.splice(io.csockets.indexOf(socket), 1);
            });
          } catch (e) {
            console.error(e.stack);
          }

        });
//        import io from 'socket.io'(server);

        app.use(cookieParser());
        app.use(bodyParser.json({limit:'1gb'}));         // to support JSON-encoded bodies
        app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
          extended: true,
          limit:'1gb'
        }));

        app.use(compression())

        return new Router(app, server, io, { host: host, port: port });
      } catch (e) {
        console.error(e.stack);
      }
    }
    constructor(app, server, io, cfg) {
      this.app = app;
      this.server = server;
      this.io = io;
      this.clients = [];
      this.host = cfg.host;
      this.port = cfg.port;

      this.used_get_paths = [];
      this.used_post_paths = [];
    }

    async listen(admin_auth) {
      try {
        let this_class = this;
        let io = this.io;

        if (this.instance) {
          this.instance.close();
        }

/*        if (admin_auth) {
          io.use(async function(socket, next){
            try {
              if (socket.request.headers.cookie) {
                socket.request.cookies = cookie.parse(socket.request.headers.cookie);
                console.log("SOCKET", socket.request.headers);

                // Authorization
                if (socket.request.cookies.access_token) {
                  let client_exists = false;

                  this_class.clients.forEach(client => {
                    if (socket.request.cookies.access_token === client.jwt) {
                      client_exists = true;
                    }
                  });

                  if (!client_exists) {
                    let n_client = new Client(socket, socket.request.cookies.access_token);
                    this_class.clients.push(n_client);
                    this.clients_cache = [];

                    socket.on('disconnect', function() {
                      console.log("disconnect", this_class.clients.indexOf(n_client));
                      this_class.clients.splice(this_class.clients.indexOf(n_client), 1);
                    });
                  }

                  let payload = await admin_auth.decrypt_token(socket.request.cookies.access_token);
                  if (payload) {
                    next();
                  }
                } else {
                  unauthorized();
                }
              } else {
                unauthorized();
              }

              function unauthorized() {
                let err  = new Error('Authentication error');
                err.data = { type : 'authentication_error' };
                next(err);
              }
            } catch(e) {
              console.error(e.stack);
            }
          });
        }*/

        let server = this.server;

        return await new Promise((resolve, reject) => {
          this_class.instance = server.listen(this_class.port, this_class.host, function() {
            var addr = server.address();
            console.log("\x1b[32mServer listening on:\x1b[36m", addr.address + ":" + addr.port+"\x1b[0m");
            resolve();
          });
        });
      } catch (e) {
        console.error(e.stack);
        return undefined;
      }
    }

    up_index(method, up_path) {
      if (method == "GET") {
        for (let p = 0; p < this.used_get_paths.length; p++) {
          let used_path = this.used_get_paths[p];
          if (used_path.path == up_path) {
            return p;
          }
        }
      } else if (method == "POST") {
        for (let p = 0; p < this.used_post_paths.length; p++) {
          let used_path = this.used_post_paths[p];
          if (used_path.path == up_path) {
            return p;
          }
        }
      }
      return undefined;
    }

    get(gpath, ...callbacks) {
      let _this = this;
      let up_index = this.up_index("GET", gpath);
      if (up_index || up_index == 0) {
        _this.used_get_paths[up_index].cbs = callbacks;
      } else {
        let used_path = {
          path: gpath,
          cbs: callbacks
        }
        up_index = this.used_get_paths.length;
        this.used_get_paths.push(used_path);

        async function call_cbs(req, res, next) {
          try {
            for (let c = 0; c < _this.used_get_paths[up_index].cbs.length; c++) {
              await new Promise(function(resolve) {
                if (c != _this.used_get_paths[up_index].cbs.length-1) {
                  _this.used_get_paths[up_index].cbs[c](req, res, resolve);
                } else {
                  _this.used_get_paths[up_index].cbs[c](req, res);
                  resolve();
                }
              });
            }
          } catch (e) {
            console.error(e.stack);
          }
        }
        this.app.get(gpath, call_cbs);
      }
    }

    get_command(gpath, callbacks, ...middlewares) {
      middlewares = [...middlewares, function(req, res) {
        let data = JSON.parse(req.query.data);

        for (let cb in callbacks) {
          if (cb == data.command) {
            delete data.command;
            callbacks[cb](req, res, data);
            return;
          }
        }

        throw new Error("Invalid command: "+data.command);
      }];
      this.get(gpath, ...middlewares);
    }

    post(ppath, ...callbacks) {
      let _this = this;
      let up_index = this.up_index("POST", ppath);
      if (up_index || up_index == 0) {
        _this.used_post_paths[up_index].cbs = callbacks;
      } else {
        let used_path = {
          path: ppath,
          cbs: callbacks
        }
        up_index = this.used_post_paths.length;
        this.used_post_paths.push(used_path);

        async function call_cbs(req, res, next) {
          try {
            for (let c = 0; c < _this.used_post_paths[up_index].cbs.length; c++) {
              await new Promise(function(resolve) {
                if (c != _this.used_post_paths[up_index].cbs.length-1) {
                  _this.used_post_paths[up_index].cbs[c](req, res, resolve);
                } else {
                  _this.used_post_paths[up_index].cbs[c](req, res);
                  resolve();
                }
              });
            }
          } catch (e) {
            console.error(e.stack);
          }
        }
        this.app.post(ppath, call_cbs);
      }
    }

    post_command(ppath, callbacks, ...middlewares) {
      middlewares = [...middlewares, function(req, res) {
        let data = JSON.parse(req.body.data);

        for (let cb in callbacks) {
          if (cb == data.command) {
            delete data.command;
            callbacks[cb](req, res, data);
            return;
          }
        }

        throw new Error("Invalid command: "+data.command);
      }];
      this.post(ppath, ...middlewares);
    }

    post_file(ppath, cfg, ...middlewares) {
      let storage = multer.diskStorage({
        destination: async function (req, file, cb) {
          try {
            let result = await cfg.destination(req, file);
            cb(null, result);
          } catch (e) {
            console.error(e.stack);
          }
        },
        filename: async function (req, file, cb) {
          try {
            let result = await cfg.filename(req, file);
            cb(null, result);
          } catch (e) {
            console.error(e.stack);
          }
        }
      });

      let upload = multer({ storage: storage })
      middlewares = [...middlewares, upload.array(cfg.name), cfg.saved];
      this.post(ppath, ...middlewares);
    }

    serve(dir_path) {
      this.app.use(express.static(dir_path));
    }

    use(...args) {
      this.app.use(...args);
    }

    static static(dir_path) {
      return express.static(dir_path);
    }

    close(func_name) {
      var routes = this.app._router.stack;
      routes.forEach(removeMiddlewares);
      function removeMiddlewares(route, i, routes) {
        switch (route.handle.name) {
          case func_name:
            routes.splice(i, 1);
        }
        if (route.route)
          route.route.stack.forEach(removeMiddlewares);
      }
    }
}
