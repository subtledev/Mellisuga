
import path from 'path';
import fs from 'fs-extra';

function isClass(v) {
  return typeof v === 'function' && /^\s*class\s+/.test(v.toString());
}

import { ESLint } from "eslint";

export default class Module {
  constructor(cfg, cms) {
    this.name = cfg.name;
    this.full_path = cfg.full_path;
    this.config = cfg.config;

    let extra_cfg = cms.get_module_cfg(this.name);
    this.config = extra_cfg ? { ...this.config, ...extra_cfg } : this.config;
    this.parent_list =  cfg.parent_list
    this.cms = cms;


    this.cur_watching = [];
    if (cfg.dev_mode) {
      this.watch_module_dir(this.full_path);

    }
  }

  static async init(cfg, cms) {
    try {
      let this_class = new Module(cfg, cms);


      let report_time = new Date();
      report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);
      console.log("\x1b[34m"+report_time, "\x1b[1m\x1b[33mMODULE\x1b[0m --->\x1b[36m", this_class.full_path, "\x1b[0m");


      let this_module = undefined;
      try {
        console.log("IMPORT", this_class.full_path+"/index.js");
        this_module = await import(this_class.full_path+"/index.js");
      } catch (e) {
        const eslint = new ESLint({
          baseConfig: {
            ignorePatterns: ["**/dist/*.js"],
            parser: "@babel/eslint-parser",
            parserOptions: {
              requireConfigFile: false
            },
          }
        });
        const results = await eslint.lintFiles([this_class.full_path+"/**/*.js"]);
        const formatter = await eslint.loadFormatter("stylish");
        const resultText = formatter.format(results);
        console.log(resultText || e);
        return undefined;
      }

      if (this_module.default.init) {
        this_class.object = await (this_module.default.init(cms, this_class.config));
      } else if (this_module.default.construct) {
        this_class.object = await (this_module.default.construct(cms, this_class.config));
      }
      return this_class;
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  watch_module_dir(dir_path) {
    this.cur_watching.push(dir_path);

    let _this = this;
    fs.watch(dir_path, {}, function(eventType, filename) {
      console.log("FILENAME", filename);
      if (typeof filena !== 'string' || !filename.endsWidth(".js")) return;
      if (!_this.ignore_changes) {
        _this.ignore_changes = true;
        setTimeout(function() {
          _this.reload();
          _this.ignore_changes = false;
        }, 1000);
      }
      let chpath = path.resolve(dir_path, filename);
      if (fs.existsSync(chpath)) {
        if (fs.lstatSync(chpath).isDirectory() && !_this.cur_watching.includes(chpath)) {
          _this.watch_module_dir(chpath); 
        }
      } else {
        if (_this.cur_watching.includes(chpath)) {
          fs.unwatchFile(chpath);
          _this.cur_watching = _this.cur_watching.filter(item => item !== chpath);
        }
      }
    });
    let files = fs.readdirSync(dir_path, "utf8", true);
    for (let f = 0; f < files.length; f++) {
      let file = path.resolve(dir_path, files[f]);
      if (fs.lstatSync(file).isDirectory()) {
        this.watch_module_dir(file)
      }
    }
  }

  stop_watching() {
    for (let w = 0; w < this.cur_watching.length; w++) {
      let cw = this.cur_watching[w];
      fs.unwatchFile(cw);
    }
    this.cur_watching = [];
  }

  data() {
    let parent_list = "unlisted";

    if (this.parent_list) {
      parent_list = this.parent_list.name;
    }


    return {
      name: this.name,
      full_path: this.full_path,
      config: this.config,
      parent_list: parent_list
    }
  }

  async reload() {
    try {
      if (typeof this.object.destroy === 'function') this.object.destroy();
      delete this.object;
      let report_time = new Date();
      report_time = report_time.toDateString() + " " + report_time.toTimeString().slice(0, report_time.toTimeString().indexOf("GMT")-1);
      console.log("\x1b[34m"+report_time, "\x1b[33mMODULE --->\x1b[36m", this.full_path, "\x1b[0m");

      let extra_cfg = this.cms.get_module_cfg(this.name);
      this.config = extra_cfg ? { ...this.config, ...extra_cfg } : this.config;

      delete require.cache[require.resolve(this.full_path)];
      let nmodule = require(this.full_path);
      if (nmodule.construct) {
        this.object = await nmodule.construct(this.cms, this.config);
      } else if (nmodule.init) {
        this.object = await nmodule.init(this.cms, this.config);
      } else if (isClass(nmodule)) {
        this.object = new nmodule(this.cms, this.config);
      } else {
        console.warn("Module `", this.full_path, "` does not have any construction functions!");
      }
    } catch (e) {
      console.error(e.stack);
      return undefined;
    }
  }

  destroy() {
    this.destroyed = true;
    fs.removeSync(this.full_path);
  }
}
