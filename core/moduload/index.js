import fs from 'fs-extra';
import path from 'path';

import Controls from './controls.js';
import ModuleList from './module_list.js';
import Module from './module.js';

/**
 * Loads modules within {@link Mellisuga#app_path}/mellisuga_modules/(core|community)/waf/ directory
 * @class Moduload
 * @hideconstructor
 */
export default class ModulesIO {
  /**
   * @static
   * @async
   * @method construct
   * @memberof Moduload
   * @arg {Mellisuga} waf Mellisuga web application framework object
   * @arg {Object} cfg Configuration 
   * @returns {Moduload}
   */
  static async construct(cms, cfg) {
    const mod_path = path.resolve(cms.app_path, 'mellisuga_modules');
    if (!fs.existsSync(mod_path)) {
      fs.mkdirSync(mod_path);
    }

    const modules_path = path.resolve(mod_path, 'waf');
    if (!fs.existsSync(modules_path)) fs.mkdirSync(modules_path);

    let this_class = new ModulesIO(modules_path, cms, cfg);

    const core_path = path.resolve(modules_path, 'core');
    if (!fs.existsSync(core_path)) fs.mkdirSync(core_path);
    this_class.load_in_dir(core_path);

    const community_path = path.resolve(modules_path, 'community');
    if (!fs.existsSync(community_path)) fs.mkdirSync(community_path);
    this_class.load_in_dir(community_path);

    return this_class;
  }

  constructor(modules_path, cms, cfg) {
    this.dir = modules_path;
    this.cms = cms;
    this.cfg = cfg;
  }

  init_controls(auth) {
    this.controls = new Controls({
      command_path: '/mellisuga/modules.io',
      auth: auth 
    }, this.cms);
  }

  async load_in_dir(full_path) {
    if (!full_path) full_path = this.dir;
    this.module_lists = [];
    let module_list = await ModuleList.init(full_path, this.cms, this.cfg);


    this.module_lists.push(module_list);
    return module_list;
  };

  async load_after(what, cms) {
    try {

      this.cms = cms;
      for (let l = 0; l < this.module_lists.length; l++) {
        let mlist = this.module_lists[l];

        for (let m = 0; m < mlist.all_modules.length; m++) {
          if (mlist.all_modules[m].config && mlist.all_modules[m].config.after === what) {
            let modl = await Module.init({ ...this.cfg, ...mlist.all_modules[m] }, this.cms);
            mlist.list.push(modl);
            mlist.all_modules[m].loaded = true;
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  async load_all(cms) {
    try {
      this.cms = cms;
      for (let l = 0; l < this.module_lists.length; l++) {
        let mlist = this.module_lists[l];

        for (let m = 0; m < mlist.all_modules.length; m++) {
          if (!mlist.all_modules[m].loaded) {
            let modl = await Module.init({ ...this.cfg, ...mlist.all_modules[m] }, this.cms);
            mlist.list.push(modl);
            mlist.all_modules[m].loaded = true;
          }
        }
      }
    } catch (e) {
      console.error(e.stack);
    }
  }

  all() {
    let obj = {};

    for (let l = 0; l < this.module_lists.length; l++) {
      let module_list = this.module_lists[l];
      obj[module_list.name] = [];
      for (let m = 0; m < module_list.list.length; m++) {
        obj[module_list.name].push(module_list.list[m].data());
      }
    }
/*
    obj.unlisted = [];
    for (let p = 0; p < this.list.length; p++) {
      if (!this.list[p].dev_only || this.list[p].dev_only && this.cmbird.dev_mode) {
        obj.unlisted.push(this.list[p].data());
      }
    }
*/

    return obj;
  }

  select_list_obj(list_name) {
    for (let l = 0; l < this.module_lists.length; l++) {
      if (!this.module_lists[l].dev_only || this.module_lists[l].dev_only && this.cms.dev_mode) {
        if (this.module_lists[l].name === list_name) {
          return this.module_lists[l];
        }
      }
    }
    return undefined;
  }

  module_exists(mname) {
    for (let l = 0; l < this.module_lists.length; l++) {
      if (this.module_lists[l].module_exists(mname)) {
        return true;
      }
    }
    return false;
  }

  module_loaded(mname) {
    for (let l = 0; l < this.module_lists.length; l++) {
      if (this.module_lists[l].module_loaded(mname)) {
        return true;
      }
    }
    return false;
  }

  get_module(mname) {
    for (let l = 0; l < this.module_lists.length; l++) {
      if (this.module_lists[l].module_loaded(mname)) {
        return this.module_lists[l].get_module(mname);
      }
    }

    console.log("Can't get module `"+mname+"` object. It's not loaded!");
    return false;
  }
}
