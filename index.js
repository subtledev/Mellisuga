

import CORE from './core/index.js';

export default class {

  static async construct(cfg) {
    try {
      cfg.cms_path = new URL('.', import.meta.url).pathname;
      return await CORE.construct(cfg);
    } catch (e) {
      console.error(e);
      return undefined;
    }
  }
}
